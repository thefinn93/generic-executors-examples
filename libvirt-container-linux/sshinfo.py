#!/usr/bin/env python3
# consolecallback - provide a persistent console that survives guest reboots
import re
import sys
import subprocess
import libvirt

ssh_regex = re.compile(r"SSH host key: (?P<fp>SHA256:[a-zA-Z0-9+\/]*) \((?P<algo>\w*)\)")


def error_handler(unused, error):
    # The console stream errors on VM shutdown; we don't care
    if (error[0] == libvirt.VIR_ERR_RPC and
        error[1] == libvirt.VIR_FROM_STREAMS):
        return
    sys.exit(1)


class Console(object):
    last_line = ""
    keys = []
    ips = []

    def __init__(self, uri, uuid):
        self.uri = uri
        self.uuid = uuid
        self.connection = libvirt.open(uri)
        self.domain = self.connection.lookupByUUIDString(uuid)
        self.state = self.domain.state(0)
        self.connection.domainEventRegister(lifecycle_callback, self)
        self.stream = None
        self.run_console = True

    def checkline(self):
        self.last_line = self.last_line.strip()
        ssh = ssh_regex.match(self.last_line)
        if ssh is not None:
            self.keys.append((ssh.group('algo'), ssh.group('fp')))
        elif self.last_line.startswith("SSH"):
            print("ssh line that didn't pass the filter: \"%s\"" % self.last_line, file=sys.stderr)
        elif self.last_line.startswith("eth"):
            device, ips = self.last_line.split(": ", 2)
            ips = [ip.strip() for ip in ips.split(" ")]
            self.ips += ips
        elif len(self.ips) > 0:
            if self.last_line == "":
                for ip in self.ips:
                    if ip != "":
                        build_known_hosts(ip, self.keys)
                sys.exit()
        self.last_line = ""


def build_known_hosts(ip, keys):
    # Unfortunately, the SSH fingerprints from Container Linux don't work in known_hosts files,
    # so we scan the server to get the full keys, then verify them.
    keyscan = subprocess.run(["ssh-keyscan", ip], check=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL).stdout.decode().split("\n")
    for key in keyscan:
        if key == "":
            continue
        fp = subprocess.run(["ssh-keygen", "-lf", "-"], check=True, stdout=subprocess.PIPE, input=key.encode('utf-8')).stdout.decode().strip().split(" ")
        if (fp[3][1:-1], fp[1]) in keys and key != "":
            print(key)


def check_console(console):
    if (console.state[0] == libvirt.VIR_DOMAIN_RUNNING or
        console.state[0] == libvirt.VIR_DOMAIN_PAUSED):
        if console.stream is None:
            console.stream = console.connection.newStream(libvirt.VIR_STREAM_NONBLOCK)
            console.domain.openConsole(None, console.stream, 0)
            console.stream.eventAddCallback(libvirt.VIR_STREAM_EVENT_READABLE, stream_callback, console)
    else:
        sys.exit()

    return console.run_console


def stream_callback(stream, events, console):
    try:
        received_data = console.stream.recv(1024).decode()
    except:
        return
    while "\n" in received_data:
        rest_of_line, received_data = received_data.split("\n", 1)
        console.last_line += rest_of_line
        console.checkline()
    console.last_line += received_data


def lifecycle_callback(connection, domain, event, detail, console):
    console.state = console.domain.state(0)


# main
if len(sys.argv) != 3:
    print("Usage: %s %s" % (sys.argv[0], "URI UUID"))
    print("for example: %s" % (sys.argv[0], "qemu:///system 32ad945f-7e78-c33a-e96d-39f25e025d81"))
    sys.exit(1)

uri = sys.argv[1]
uuid = sys.argv[2]

libvirt.virEventRegisterDefaultImpl()
libvirt.registerErrorHandler(error_handler, None)

console = Console(uri, uuid)

while check_console(console):
    libvirt.virEventRunDefaultImpl()

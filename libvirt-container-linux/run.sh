#!/bin/bash
set -euo pipefail
BUILD_ID="${CI_PROJECT_PATH_SLUG}-${CI_JOB_ID}"
VM_IP=$(awk '{ print $1 }' /var/lib/libvirt/gitlab-runner/${BUILD_ID}.ssh_hosts | head -n 1)

sed "s#$HOME#/home/core#g" | ssh -l core -o "UserKnownHostsFile=/var/lib/libvirt/gitlab-runner/${BUILD_ID}.ssh_hosts" -o "HashKnownHosts=no" "${VM_IP}" bash

To use this, copy all of the files here to `/etc/gitlab-runner`. You'll probably
need to do something with the config.toml file, I took all my tokens and stuff out.

These scripts depend on the following debian packages:

```
python3 python3-libvirt curl libvirt-clients xmlstarlet virtinst qemu-utils libguestfs-tools
```

and the [Container Linux config transpiler](https://github.com/coreos/container-linux-config-transpiler).


You'll need to create these folders and give the GitLab runner user access to them:

* `/var/lib/libvirt/gitlab-runner/`
* `/var/gitlab-runner/images/`

Then run `/etc/gitlab-runner/update-coreos.sh` to download and verify the latest
version of Container Linux. That script can be configured to run on a cron job,
to ensure you're always on the latest version.

Generate an SSH key for your runner user if you haven't already, and put it's
public key in `ignition-template.yaml`, replacing my runner's key.


There's probably something that I've missed. Open an issue or MR if you find it!

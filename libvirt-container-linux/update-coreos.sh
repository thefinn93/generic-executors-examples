#!/bin/bash
set -exuo pipefail
TEMPDIR=$(mktemp -dt coreos-download-XXXXXX)

cleanup() {
    rm -rfv $TEMPDIR
}
trap cleanup EXIT
pushd ${TEMPDIR}
wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_qemu_image.img.bz2
wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_qemu_image.img.bz2.sig
GNUPGHOME=${TEMPDIR} gpg2 --verify --no-default-keyring --keyring "/etc/gitlab-runner/coreos.gpg" coreos_production_qemu_image.img.bz2.sig
bzip2 -d coreos_production_qemu_image.img.bz2
qemu-img resize coreos_production_qemu_image.img +20G
popd
cp "${TEMPDIR}/coreos_production_qemu_image.img" /var/installmedia/gitlab_runner_coreos.img

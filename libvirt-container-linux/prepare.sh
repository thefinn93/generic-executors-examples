#!/bin/bash
set -euo pipefail
BUILD_ID="${CI_PROJECT_PATH_SLUG}-${CI_JOB_ID}"

domain="/var/lib/libvirt/gitlab-runner/${BUILD_ID}.xml"
ignition_file="/var/lib/libvirt/gitlab-runner/${BUILD_ID}.ign"

start=$(date +%s)

qemu-img create -f qcow2 -b /var/installmedia/gitlab_runner_coreos.img "/var/gitlab-runner/images/gitlab-runner-${BUILD_ID}.qcow2" 20G

echo write /grub.cfg \"set linux_append=\\\"\$linux_append coreos.autologin=tty0 coreos.autologin=ttyS0\\\"\" | guestfish -a "/var/gitlab-runner/images/gitlab-runner-${BUILD_ID}.qcow2" -m /dev/sda6

virt-install --connect qemu:///system -n "gitlab-job-${BUILD_ID}" --memory 2048 --vcpus 4 --import --disk "/var/gitlab-runner/images/gitlab-runner-${BUILD_ID}.qcow2,format=qcow2,bus=virtio" --print-xml --os-type=linux --os-variant=virtio26 > "${domain}"

sed "s#BUILD_ID#${BUILD_ID}#g" /etc/gitlab-runner/ignition-template.yaml | ct > "$ignition_file"

xmlstarlet ed -P -L -i "//domain" -t attr -n "xmlns:qemu" --value "http://libvirt.org/schemas/domain/qemu/1.0" "${domain}"
xmlstarlet ed -P -L -s "//domain" -t elem -n "qemu:commandline" "${domain}"
xmlstarlet ed -P -L -s "//domain/qemu:commandline" -t elem -n "qemu:arg" "${domain}"
xmlstarlet ed -P -L -s "(//domain/qemu:commandline/qemu:arg)[1]" -t attr -n "value" -v "-fw_cfg" "${domain}"
xmlstarlet ed -P -L -s "//domain/qemu:commandline" -t elem -n "qemu:arg" "${domain}"
xmlstarlet ed -P -L -s "(//domain/qemu:commandline/qemu:arg)[2]" -t attr -n "value" -v "name=opt/com.coreos/config,file=${ignition_file}" "${domain}"

virsh -c qemu:///system define "${domain}"
until virsh -c qemu:///system start "gitlab-job-${BUILD_ID}"; do
  # When two jobs are started concurrently, a race condition occurs and one of them fails, so this loop just runs it again.
  sleep 1;
done

# Wait for the system to boot, scrape the SSH keys and IP address from the console and write them to the known_hosts file
python3 /etc/gitlab-runner/sshinfo.py qemu:///system $(xmlstarlet sel -t -v "/domain/uuid" "${domain}") > "/var/lib/libvirt/gitlab-runner/${BUILD_ID}.ssh_hosts"

VM_IP=$(head -n 1 /var/lib/libvirt/gitlab-runner/${BUILD_ID}.ssh_hosts | awk '{ print $1 }')

until nc -q 0 "${VM_IP}" 22 < /dev/null; do
 sleep 1
done

time=$(($(date +%s) - $start))

echo "VM booted in $time seconds"

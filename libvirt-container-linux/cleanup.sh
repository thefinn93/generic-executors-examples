#!/bin/bash
set -exuo pipefail
BUILD_ID="${CI_PROJECT_PATH_SLUG}-${CI_JOB_ID}"

virsh -c qemu:///system destroy "gitlab-job-${BUILD_ID}" || true
virsh -c qemu:///system undefine "gitlab-job-${BUILD_ID}" || true

rm -f "/var/gitlab-runner/images/gitlab-runner-${BUILD_ID}.qcow2" "/tmp/gitlab-jobs/${BUILD_ID}" "/var/lib/libvirt/gitlab-runner/${BUILD_ID}.ign" "/var/lib/libvirt/gitlab-runner/${BUILD_ID}.xml" "/var/lib/libvirt/gitlab-runner/${BUILD_ID}.ssh_hosts"
